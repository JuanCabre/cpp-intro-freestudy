import IPython.core.magic as ipym

@ipym.magics_class
class CppMagics(ipym.Magics):
    @ipym.cell_magic
    def cpp(self, line, cell=None):
        """Compile, execute C++ code, and return the standard output."""
        # Define the source and executable filenames.
        source_filename = 'temp.cpp'
        program_filename = './temp'
        # Write the code contained in the cell to the C++ file.
        with open(source_filename, 'w') as f:
            f.write(cell)
        # Compile the C++ code into an executable.
        compile = self.shell.getoutput("g++ -std=c++11 {0:s} -pthread -o {1:s}".format(
            source_filename, program_filename))

        # If the compiler makes an error we just return that error
        if len(compile) > 1:
            return compile

        # Execute the executable and return the output.
        output = self.shell.getoutput(program_filename)
        return output

@ipym.magics_class
class Cpp14Magics(ipym.Magics):
    @ipym.cell_magic
    def cpp14(self, line, cell=None):
        """Compile, execute C++ code, and return the standard output."""
        # Define the source and executable filenames.
        source_filename = 'temp.cpp'
        program_filename = './temp'
        # Write the code contained in the cell to the C++ file.
        with open(source_filename, 'w') as f:
            f.write(cell)
        # Compile the C++ code into an executable.
        compile = self.shell.getoutput("g++ -std=c++14 {0:s} -pthread -o {1:s}".format(
            source_filename, program_filename))

        # If the compiler makes an error we just return that error
        if len(compile) > 1:
            return compile

        # Execute the executable and return the output.
        output = self.shell.getoutput(program_filename)
        return output

def load_ipython_extension(ipython):
    ipython.register_magics(CppMagics)
    ipython.register_magics(Cpp14Magics)
