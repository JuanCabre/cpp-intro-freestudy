#include <iostream>

constexpr unsigned factorial(unsigned n)
{
    //int *a = new int;
    return n <= 1 ? 1 : (n * factorial(n-1));
}

int main()
{
    volatile unsigned a = 8U;
    volatile unsigned sum = 0;

    for(uint32_t i = 0; i < 1000000000; ++i)
    {
        //sum += factorial(8U);
        sum += factorial(a);
    }

    std::cout << "value = " << sum << std::endl;
}
