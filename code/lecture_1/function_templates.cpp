#include <iostream>
#include <string>
#include <vector>
#include <map>

class math_lecture
{
public:

    bool take_exam()
    {
        std::cout << "What is 2 + 2" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 4;
    }
};

class history_lecture
{
public:

    bool take_exam()
    {
        std::cout << "When was the first C++ standard released" << std::endl;
        int answer;
        std::cin >> answer;

        return answer == 1998;
    }
};

template<class T>
bool pre_exam(T &t)
{
    std::cout << "Next C++ standard major release" << std::endl;
    int answer;
    std::cin >> answer;

    if(answer == 2017)
    {
        return t.take_exam();
    }
    else
    {
        return false;
    }
}



int main()
{
    math_lecture math;
    history_lecture history;

    std::cout << pre_exam(math) << std::endl;
    std::cout << pre_exam(history) << std::endl;

    return 0;
}
